$ npm install -g newman
$ newman run JobPosting.postman_collection.json
newman

JobPosting

→ ค้าหางานด้วย Keyword "Programmer"
  GET http://localhost:3000/jobPostings/search/Programmer [200 OK, 596B, 96ms]
  √  Status code is 200

→ ดูรายละเอียดงาน เลือก id 1Python Programmer
  GET http://localhost:3000/jobPostings/1 [200 OK, 351B, 10ms]
  √  Status code is 200
  1. Your test name

→ สมัครงาน ตามเงือนไขทั้งหมด
  POST http://localhost:3000/jobPostings/1/apply/ [200 OK, 482B, 14ms]
  2. Status code is 200

→ สมัครงาน อายุก่อนกำหนด
  POST http://localhost:3000/jobPostings/1/apply/ [422 Unprocessable Entity, 505B, 7ms]
  √  Status code is 422

→ สมัครงาน ประสบการณ์น้อยกว่าที่กำหนด
  POST http://localhost:3000/jobPostings/1/apply/ [422 Unprocessable Entity, 511B, 7ms]
  √  Status code is 422

→ สมัครงาน เพศตรงไม่ตรงตามเงื่อนไข
  POST http://localhost:3000/jobPostings/1/apply/ [422 Unprocessable Entity, 525B, 7ms]
  √  Status code is 422

┌─────────────────────────┬──────────────────┬──────────────────┐
│                         │         executed │           failed │
├─────────────────────────┼──────────────────┼──────────────────┤
│              iterations │                1 │                0 │
├─────────────────────────┼──────────────────┼──────────────────┤
│                requests │                6 │                0 │
├─────────────────────────┼──────────────────┼──────────────────┤
│            test-scripts │                6 │                0 │
├─────────────────────────┼──────────────────┼──────────────────┤
│      prerequest-scripts │                0 │                0 │
├─────────────────────────┼──────────────────┼──────────────────┤
│              assertions │                7 │                2 │
├─────────────────────────┴──────────────────┴──────────────────┤
│ total run duration: 821ms                                     │
├───────────────────────────────────────────────────────────────┤
│ total data received: 1.46KB (approx)                          │
├───────────────────────────────────────────────────────────────┤
│ average response time: 23ms [min: 7ms, max: 96ms, s.d.: 32ms] │
└───────────────────────────────────────────────────────────────┘

  #  failure                                       detail

 1.  AssertionError                                Your test name
                                                   expected ' Python Programmer' to deeply equal 'Python Programmer'
                                                   at assertion:1 in test-script
                                                   inside "ดูรายละเอียดงาน เลือก id 1Python Programmer"

 2.  AssertionError                                Status code is 200
                                                   expected response to have status code 201 but got 200
                                                   at assertion:0 in test-script
                                                   inside "สมัครงาน ตามเงือนไขทั้งหมด"